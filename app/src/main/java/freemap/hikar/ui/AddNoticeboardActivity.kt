package freemap.hikar.ui

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.preference.PreferenceManager
import kotlinx.android.synthetic.main.input_noticeboard.*
import kotlinx.coroutines.*
import android.util.Base64
import freemap.hikar.R
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class AddNoticeboardActivity : ProgressBarActivity() {

    override val progressBarId = R.id.progressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.input_noticeboard)

        btnOkInputAnnotation.setOnClickListener {
            val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
            val email = prefs.getString("email", "") ?: ""
            val password = prefs.getString("password", "") ?: ""
            val lat = intent.getDoubleExtra("lat", Double.MAX_VALUE)
            val lon = intent.getDoubleExtra("lon", Double.MAX_VALUE)
            val annotationType = spAnnotationType.selectedItemPosition*2 + 1
            if(email != "" && password != "" && (-90.0..90.0).contains(lat) && (-180.0..180.0).contains(lon)) {
                sendDetails(email, password, lat, lon, annotationType, etAnnotation.text.toString())
            } else {
                AlertDialog.Builder(this@AddNoticeboardActivity).setPositiveButton("OK", null).setMessage("Email and password not specified, you need to specify them in the Preferences.").show()
            }
        }

        btnCancelInputAnnotation.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
    }

    private fun sendDetails(email: String, password: String, lat: Double, lon: Double, annotationType: Int, annotation: String) {

        launch {
            var msg : String
            var resp = ""
            try {
                val postData="action=create&lon=$lon&lat=$lat&annotationType=$annotationType&text=$annotation"
                var status = 200
                showProgressBar(true)
                withContext(Dispatchers.IO) {
                    val url = URL("https://hikar.org/fm/ws/annotation.php")
                    val conn = url.openConnection() as HttpURLConnection
                    val details="$email:$password"
                    with (conn) {
                        setRequestProperty(
                            "Authorization",
                            "Basic ${Base64.encodeToString(details.toByteArray(), Base64.NO_WRAP)}"
                        )
                        doOutput = true
                        setFixedLengthStreamingMode(postData.length)
                        outputStream.write(postData.toByteArray())
                        status = responseCode

                        if(status==200) {

                            val reader = BufferedReader(InputStreamReader(inputStream))
                            var line: String? = reader.readLine()
                            while (line != null) {
                                resp += line
                                line = reader.readLine()
                            }
                        }
                    }
                }

                msg = when(status) {
                    200 -> "Successfully added noticeboard, however it will need to be approved by administrators."
                    401 -> "Incorrect login details."
                    else -> "Server error: code $status"
                }
            } catch (e: Exception) {
                msg = "An error occurred; this is probably due to no network connection. Details: $e"
            }
            if(!job.isCancelled) {
                showProgressBar(false)
                AlertDialog.Builder(this@AddNoticeboardActivity).setPositiveButton("OK") { _, _ -> finish() }
                    .setMessage(msg).show()
            }
        }
    }
}
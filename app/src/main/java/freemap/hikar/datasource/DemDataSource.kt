/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.datasource

import freemap.data.GoogleProjection
import freemap.datasource.TiledData
import java.io.File
import java.io.FileOutputStream
import java.net.URL

class DemDataSource(parameterisedUrl: String) : XYZTileDeliverer.TiledDataSource(parameterisedUrl) {


    override fun loadFromFile(f: File, t: GoogleProjection.Tile) : TiledData {
        return TerrariumReader(t).getData(f.inputStream())
    }

    override fun doLoadFromWeb(url: String, t: GoogleProjection.Tile, cacheFile: File) : TiledData? {
        val bytes = URL(url).openStream().use { it.readBytes() }
        FileOutputStream(cacheFile).use { it.write(bytes) }
        return TerrariumReader(t).getData(bytes)
    }
}
/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.signposting

import freemap.data.Annotation
import freemap.hikar.opengl.ModelTextOverlay
import kotlin.math.roundToInt

class NoticeBoard(
    val a: Annotation,
    val distMetres: Double,
    private val bearing: Double,
    private val nLines: Int = 2,
    private val doubleSided: Boolean = false
) : SignPlate {
    override fun getText(): Array<String> {
        val words = (a.description + if (distMetres > 0) " ${((distMetres / 20).roundToInt()) * 20}m ahead" else " - HERE").split(" ")
        val formattedWords = Array(nLines) { "" }
        var charIdx = 0
        var wordIdx = 0
        var lineIdx = 0

        while (lineIdx < nLines - 1) {
            while (charIdx < (a.description.length / nLines) * (lineIdx + 1)) {
                formattedWords[lineIdx] += "${words[wordIdx]} "
                charIdx += words[wordIdx++].length
            }
            lineIdx++
        }
        while (wordIdx < words.size) {
            formattedWords[nLines - 1] += "${words[wordIdx++]} "
        }
        return formattedWords
    }

    override fun getAngle(): Double {
        return bearing
    }


    override fun getModelTextOverlay(): ModelTextOverlay {
        return ModelTextOverlay(-20f, 52f, 20f, 32f, -3.01f, if(doubleSided) 3.01f else 2.01f, 2.0f, if (doubleSided) 2 else 1)
    }

    override fun getSignAsset(): String {
        var asset = if (a.type == "1") "sign_plate_warning" else "sign_plate_info"
        asset += if (doubleSided) "_doublesided.obj" else ".obj"
        return asset
    }
}

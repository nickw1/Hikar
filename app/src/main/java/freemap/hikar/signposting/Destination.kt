/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.signposting

/*
 * Created by nick on 09/06/15.
 */

import java.text.DecimalFormat


// Represents a destination on a signpost.
class Destination(
    private val tags: HashMap<String, String>,
    val distance: Double,
    private val settingsProvider: SettingsProvider
) {

    interface SettingsProvider {
        var distUnit: String
        var locale: String
    }

    override fun toString(): String {
        val df = DecimalFormat("#.##")
        return "$name${iconCode ?: ""} ${df.format(distance * (if (settingsProvider.distUnit == "mi") 0.6213712 else 1.0))} ${settingsProvider.distUnit}"
    }

    private val type: String
        get() = tags["class"] ?: ""


    private val name: String
        get() {
            val defaultName = tags["name"] ?: ""
            val n = when (settingsProvider.locale) {
                "" -> defaultName
                else -> {
                    val nameLocale = tags["name:${settingsProvider.locale}"]
                    if (nameLocale == null || nameLocale == "") defaultName else nameLocale
                }
            }

            return "$n ${if (isStation() && defaultName.toLowerCase().endsWith("station")) " Station" else ""}"
        }


    private val iconCode: String?
        get() = when {

            tags["railway"] == "station" -> "\u0080 "

            tags["amenity"] == "pub" -> "\u0081 "

            tags["amenity"] == "cafe" -> "\u0082 "

            tags["amenity"] == "restaurant" -> "\u0083 "

            tags["amenity"] == "parking" -> "\u0084 "

            tags["tourism"] == "camp_site" -> "\u0085"

            arrayOf("hostel", "alpine_hut").contains(tags["tourism"]) -> "\u0086"

            tags["tourism"] == "viewpoint" -> "\u0087"

            else -> null
        }

    fun getWeightedDistance(): Double {
        return distance * getWeighting()
    }

    // ballpark weightings. needs testing in the field
    fun getWeighting(): Double {
        return when {

            listOf("city", "town").contains(tags["place"]) -> {
                0.75
            }

            tags["place"] == "village" -> {
                1.0
            }

            // "Non-tourist" peaks of limited interest to walkers, e.g. Fridays Hill, Lythe Hill. Need tagging in OSM
            tags["natural"] == "peak" && tags["peak"] == "minor" -> {
                2.0
            }

            tags["natural"] == "peak" -> {
                1.25
            }

            listOf("alpine_hut", "hostel").contains(tags["tourism"]) -> {
                1.25
            }

            tags["tourism"] == "camp_site"-> {
                1.5
            }

            listOf("hamlet", "suburb").contains(tags["place"]) -> {
                1.5 // altered from 1.0, 0.2.4 -> 0.2.5
            }

            listOf("pub", "cafe").contains(tags["amenity"]) -> {
                2.0 // pubs and cafes get double weighted distance - designed not to show loads of pubs in a town/village rather than the town/village itself
            }

            tags["place"] != null -> {
                2.0 // localities get double weighted distance
            }

            tags["tourism"] == "viewpoint" -> {
                2.0 // show viewpoints if very close relative to other POIs
            }

            isStation() -> {
                1.25 // stations
            }

            else -> {
                10.0 // anything else gets a large weighted distance
            }
        }
    }

    private fun isStation(): Boolean {
        return tags["railway"] == "station"
    }
}


/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.signposting


import freemap.data.Algorithms
import freemap.data.Annotation
import freemap.data.POI
import freemap.data.Point
import freemap.datasource.OSMTiles
import freemap.hikar.datasource.OsmDemIntegrator
import kotlin.math.min


fun POI.isForSignpost(): Boolean {
    return containsKey("name") && !(getValue("name").isNullOrBlank()) && // 0.2.5 added blank string check
            (containsKey("amenity") && arrayOf("pub", "cafe", "parking", "restaurant").contains(getValue("amenity")) ||
                    containsKey("place") ||
                    containsKey("tourism") && arrayOf("hostel", "camp_site", "alpine_hut","viewpoint").contains(getValue("tourism")) ||
                    (containsKey("natural") && getValue("natural") == "peak") ||
                    (containsKey("railway") && getValue("railway") == "station"))
}

fun POI.isNearby(dist: Double): Boolean {
    return dist <= 3000.0 || (dist <= 5000.0 && !(containsKey("amenity") && getValue("amenity") == "pub") && !(containsKey(
        "place"
    ) && getValue("place") == "locality"))
}

fun POI.isLowPriority(): Boolean {
    return containsKey("place") && getValue("place") == "locality"
}

class SignpostManager : Destination.SettingsProvider {

    private var pois: OSMTiles? = null
    var graphMgr = GraphManager(0.000001, 10.0)
        private set
    private val signposts = arrayListOf<Signpost>()
    val allSigns = arrayListOf<Sign>()
    private var jMgr: JunctionManager? = null
    private val noticeBoards = HashMap<String, NoticeBoard>()

    override var locale = ""
    override var distUnit = "km"

    fun setDataset(pois: OsmDemIntegrator.KeyedOSMTiles) {
        this.pois = pois
        graphMgr.makeGraph(pois)
        jMgr = JunctionManager(graphMgr.graph)
    }

    fun update(p: Point): List<Sign> {
        if (graphMgr.graphLoaded) {
            val j = jMgr?.getJunction(p) ?: return arrayListOf()
            return onJunction(j)
        }
        return arrayListOf()
    }

    private fun onJunction(junction: Junction): List<Sign> {

        var curSignpost: Signpost? = null
        val signsToBeRendered = arrayListOf<Sign>()
        val foundNoticeBoards = hashMapOf<String, ArrayList<NoticeBoard>>()

        if (graphMgr.graphLoaded) {

            val curJunctionLoc = junction.point

            for (s in signposts) {
                if (s.position == junction.point) {
                    curSignpost = s
                    break
                }
            }

            val poiIterator = pois?.poiIterator()

            if (curSignpost == null && poiIterator != null) {

                curSignpost = Signpost(junction.point)

                // 200818 add arms at this point (so arms with no renderedRoute to POI still rendered)
                junction.ways.forEach {
                    curSignpost.addArm(Arm(it.bearing, it.tags))
                }

                var poi = poiIterator.next() as POI?
                var unprojectedPoiLoc: Point

                // poi = null // TEMP to focus on noticeboards

                while (poi != null) {
                    unprojectedPoiLoc = poi.unprojectedPoint
                //     Log.d("hikarapp", "POI name: ${poi.getValue("name")} unprojected loc = ${unprojectedPoiLoc.x} ${unprojectedPoiLoc.y}")
                    if (poi.isForSignpost()) {
                        val dist = Algorithms.haversineDist(
                            unprojectedPoiLoc.x,
                            unprojectedPoiLoc.y,
                            curJunctionLoc.x,
                            curJunctionLoc.y
                        )
                        if (poi.isNearby(dist)) {

                            val pw = graphMgr.calcRoute(curJunctionLoc, unprojectedPoiLoc)
                            if (pw.segments.size >= 2 && pw.isMostlyWalkingRoute() && !(poi.isLowPriority() && pw.distance >= 2000)) {

                                //        val bearing = p.bearingFrom(curJunctionLoc)
                                val bearing = pw.initialBearing
                                //      Log.d("hikar", "Bearing to first point in renderedRoute=$bearing")
                                val arm = curSignpost.getArmWithBearing(bearing, 15.0)
                                arm?.addDestination(Destination(poi.tags, pw.distance / 1000, this))
                            }
                        }
                    }

                    poi = poiIterator.next() as POI?

                }
                curSignpost.pruneArms()
                curSignpost.finaliseArms() // 0.2.5 added
                if (curSignpost.hasArms()) {
                    signposts.add(curSignpost)
                    signsToBeRendered.add(curSignpost)
                    allSigns.add(curSignpost)
                }
            }

            // Info signs along ways...


            val annotationIterator = pois?.annotationIterator()
            var annotation = annotationIterator?.next() as Annotation?
            var unprojectedAnnLoc: Point
            val points = hashMapOf<String, Point>()
            while (annotation != null) {

                unprojectedAnnLoc = annotation.unprojectedPoint
                //     Log.d("hikar", "SignpostManager iterating through annotations... $annotation, junction loc $curJunctionLoc unprojected ann loc=$unprojectedAnnLoc")

                val dist = Algorithms.haversineDist(
                    unprojectedAnnLoc.x,
                    unprojectedAnnLoc.y,
                    curJunctionLoc.x,
                    curJunctionLoc.y
                )
                //      Log.d("hikar", "Distance to this annotation = $dist")

                if (dist < 2000) {
                    val pw = graphMgr.calcRoute(curJunctionLoc, unprojectedAnnLoc)
                    //             Log.d("hikar", "Number of segments on path: ${pw.segments.size} Number of junctions ${pw.numJunctions}")
                    if (pw.numJunctions == 0) {
                        val p = pw.getPointOnPath(10.0)
                        //        Log.d("hikar", "Point on path = $p, curJunctionLoc=$curJunctionLoc")
                        if (p != null) {
                            // send back annotation and point...

                            if (noticeBoards["${junction.id}:${pw.segments[0].wayId}:${annotation.id}"] == null) {
                                val bearing = p.bearingFrom(curJunctionLoc) + 90
                                val iBearing = bearing.toInt()
                                val n = NoticeBoard(annotation, pw.distance, bearing, 3)
                                //          Log.d("hikar", "Adding notice board with key ${junction.id}:${pw.segments[0].wayId}:${annotation.id}")
                                //         Log.d("hikar", "Notice board formatted: ${n.getText().joinToString("\n")}")
                                //  signsToBeRendered.add(n)
                                noticeBoards["${junction.id}:${pw.segments[0].wayId}:${annotation.id}"] = n
                                if (foundNoticeBoards["${pw.segments[0].wayId}:$iBearing"] == null) {
                                    foundNoticeBoards["${pw.segments[0].wayId}:$iBearing"] = arrayListOf()
                                    points["${pw.segments[0].wayId}:$iBearing"] = p
                                }
                                foundNoticeBoards["${pw.segments[0].wayId}:$iBearing"]!!.add(n)
                            }
                        }
                    }
                    // Also place a noticeboard at the exact location
                    // This one will be double-sided.
                    if (noticeBoards["::${annotation.id}"] == null) {
                        val n = NoticeBoard(annotation, 0.0, 0.0, 3, true)
                        noticeBoards["::${annotation.id}"] = n
                        val compoundNoticeBoard = CompoundNoticeBoard(annotation.unprojectedPoint)
                        compoundNoticeBoard.addNoticeBoard(n)
                        signsToBeRendered.add(compoundNoticeBoard)
                        allSigns.add(compoundNoticeBoard)
                    }
                }
                annotation = annotationIterator?.next() as Annotation?
            }
            foundNoticeBoards.forEach { boardsForWay ->
                boardsForWay.value.sortBy { it.distMetres }
                //   Log.d("hikar", "SELECTED NOTICEBOARD WAS FOR ANNOTATION WITH ID: ${boardsForWay.value[0].a.id}")
                val compoundNoticeBoard = CompoundNoticeBoard(points[boardsForWay.key]!!)
                boardsForWay.value.subList(0, min(boardsForWay.value.size, 2)).forEach {
                    compoundNoticeBoard.addNoticeBoard(it)
                }
                //signsToBeRendered.add(boardsForWay.value[0])
                //allSigns.add(boardsForWay.value[0])
                signsToBeRendered.add(compoundNoticeBoard)
                allSigns.add(compoundNoticeBoard)
            }
        }
        return signsToBeRendered
    }

    fun clearSigns() {
        signposts.clear()
        noticeBoards.clear()
        allSigns.clear()
    }
}
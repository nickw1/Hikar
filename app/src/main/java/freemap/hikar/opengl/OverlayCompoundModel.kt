/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.opengl


class OverlayCompoundModel(x: Float, y: Float, z: Float, private val overlayGpu: TextureGPUInterface) :
    CompoundModel(x, y, z) {

    private var overlays = arrayListOf<ModelTextOverlay>()

    fun addModel(m: Model, rotation: Float, text: Array<String>, o: ModelTextOverlay, dy: Float = 0.0f) {

        super.addModel(m, rotation, dy)
        o.setText(text)
        overlays.add(o)

    }

    override fun scale(factor: Float) {
        super.scale(factor)

        overlays.forEach { it.scale(factor) }
    }

    override fun buffer() {
        super.buffer()

        overlays.forEach { it.genBuffers() }

    }

    override fun draw(gpu: GPUInterface) {

        super.draw(gpu)


        // disable depth test to force text drawing for now
        //  GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        overlayGpu.select()
        overlayGpu.sendMatrix(modelMtx, "uModelMtx")
        overlayGpu.selectTexture()

        for (i in 0 until overlays.size) {
            overlayGpu.sendMatrix(models[i + 1].rotMtx, "uArmMtx")// NB i+1 as model 0 is the post
            overlays[i].draw(overlayGpu)
        }
        //   GLES20.glEnable(GLES20.GL_DEPTH_TEST);

    }
}
